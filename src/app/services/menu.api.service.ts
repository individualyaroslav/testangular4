import { Injectable, OnInit } from '@angular/core';
import { BaseApiDataService } from '../api/base.api.data.service';
import { Category } from '../infrastructure/models/category';
import { Product } from '../infrastructure/models/product';
// data for sample
const menu = [
  {
    id: 0,
    name: 'Перші страви',
    products: [
      {
        id: 0,
        name: 'Борщ',
        price: 20,
        description: ''
      },
      {
        id: 1,
        name: 'Солянка',
        price: 30,
        description: ''
      }
    ]
   },
   {
    id: 1,
    name: 'Другі страви',
    products: [
      {
        id: 2,
        name: 'Фарширована качка',
        price: 25,
        description: ''
      },
      {
        id: 3,
        name: 'гарнір',
        price: 15,
        description: ''
      }
    ]
   },
   {
    id: 2,
    name: 'Десерти',
    products: [
      {
        id: 4,
        name: 'Морозиво в шоколаді',
        price: 10,
        description: ''
      },
      {
        id: 5,
        name: 'Морозиво в карамелі',
        price: 10,
        description: ''
      }
    ]
   },
   {
    id: 3,
    name: 'Соки',
    products: [
      {
        id: 6,
        name: 'Яблочний сік',
        price: 15,
        description: ''
      },
      {
        id: 7,
        name: 'Виноградний сік',
        price: 15,
        description: ''
      }
    ]
   }
];

const items = Promise.resolve(menu);

@Injectable()
export class MenuService {

  private menu: Category[];

  constructor( ) {
   }

  getMenu(): Promise<Category[]> {
    return items;
  }

}
