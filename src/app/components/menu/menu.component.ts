import { Component, OnInit } from '@angular/core';
import { MenuService } from '../../services/menu.api.service';
import { Category } from '../../infrastructure/models/category';
import { Product } from '../../infrastructure/models/product';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  menu: Category[];
  basket: Product[];
  summ: number;
  sortby: boolean;

  constructor(private _menuService: MenuService) {
    this.basket = [];
    this.sortby = true;
    this.summ = 0;
  }

  ngOnInit() {
    this._menuService.getMenu().then(
      result => this.menu = result
    );
  }

  addToBasket(product: Product) {
    this.basket.push(product);
    this.summ += product.price;
  }

  removeFromBasket(product: Product) {
    const index = this.basket.indexOf(product, 0);
    if (index >= 0) {
      this.basket.splice(index, 1);
    }
    this.summ -= product.price;
  }

  getSumm() {
    this.summ = 0;
    this.basket.forEach(p => {
      this.summ = this.summ + p.price;
    });
  }

  orderByCategory() {

    this.sortby ? this.menu.sort(this.compareCategory) : this.menu.sort(this.compareCategoryDesc);
    this.sortby = !this.sortby;
  }

  private compareCategory(a: Category, b: Category) {
    if (a.name > b.name) { return 1; }
    if (a.name < b.name) { return -1; }
  }

  private compareCategoryDesc(a: Category, b: Category) {
    if (a.name > b.name) { return -1; }
    if (a.name < b.name) { return 1; }
  }

}
