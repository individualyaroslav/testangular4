import { Component, OnInit, Input, OnChanges, Output, EventEmitter } from '@angular/core';
import { Product } from '../../infrastructure/models/product';

@Component({
  selector: 'app-basket',
  templateUrl: './basket.component.html',
  styleUrls: ['./basket.component.css']
})
export class BasketComponent implements OnInit {

  @Input() basket: Product[];
  @Input() summ: number;

  @Output() onRemove: EventEmitter<Product> = new EventEmitter<Product>();

  constructor() {
    this.basket = [];
    this.summ = 0;
  }

  ngOnInit() {
  }

  removeFromBasket(product: Product) {
    this.onRemove.emit(product);
  }
}
